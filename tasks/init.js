const fs = require('fs');
const stdin = process.openStdin();

console.log('Say bot name: ');

stdin.addListener('data', function(d) {
  stdin.destroy();

  const botName = d.toString().trim();

  console.log(`Your bot name will be [${botName}]`);

  fs.writeFileSync('./package.json', createBotBody(botName), errorHandler);

  console.log('Created file: ./package.json');

  fs.writeFileSync('./index.js', createBotIndex(botName), errorHandler);

  console.log('Created file: ./index.js');

  fs.writeFileSync('./README.md', createBotREADME(botName), errorHandler);

  console.log('Created file: ./README.md');

  fs.writeFileSync('./tasks/deploy.js', createDeployTask(), errorHandler);

  console.log('Created file: ./tasks/deploy.js');

  fs.unlinkSync('./tasks/init.js');

  console.log('Deleted init task');

  console.log('DONE!');
});

const createBotBody = name => `{
  "name": "${name}",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "start": "node index.js",
    "deploy": "node tasks/deploy",
    "lint": "eslint *.js --fix"
  },
  "dependencies": {
    "axios": "^0.16.2",
    "body-parser": "^1.17.2",
    "express": "^4.15.4"
  },
  "devDependencies": {
    "babel-core": "6.26.0",
    "babel-eslint": "8.2.2",
    "babel-preset-es2015": "6.24.1",
    "babel": "6.1.18",
    "eslint-config-prettier": "2.9.0",
    "eslint-plugin-prettier": "2.6.0",
    "eslint-plugin-react": "7.7.0",
    "eslint": "4.19.1",
    "prettier": "1.11.1"
  }
}
`;

const createBotIndex = name => `const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get('/', (req, res) => res.end('ok'));

app.post('/message', function messagePostHandler(req, res) {
  const { message } = req.body;
  const { token } = req.query;

  if (!token || !message) {
    return res.end('no token or no message');
  }

  const msgArray = (function splitText(message = '') {
    return message.split(/[\s, -]/);
  })(message.text);

  const hasBeenCalled = msgArray.some(word => word === '@${name}');

  if (hasBeenCalled) {
    const text = 'Hello World';

    axios
      .post(\`https://api.telegram.org/bot\$\{token\}/sendMessage\`, {
        chat_id: message.chat.id,
        reply_to_message_id: message.reply_to_message ? message.reply_to_message.message_id : message.message_id,
        text,
      })
      .then(() => res.end('ok'))
      .catch(err => res.end(\`error :\$\{err\}\`));
  } else {
    return res.end('ok');
  }
});

app.listen(PORT, function() {
  console.log(\`Telegram app listening on port \$\{PORT\}!\`);
});
`;

const createBotREADME = name => `# ${name}

Bot description

## Commands

### Starting

\`\`\`
  $ yarn start
\`\`\`

This command will start your bot as an api, you can test your bot with this command.

### Deploy

\`\`\`
  $ URL=https://bot-url.doman TOKEN=your-bot:token yarn deploy
\`\`\`

This command will set a webhook for your bot, all you need will be an url where you have deployed this api and your bot token.
`;

const createDeployTask = () => `const axios = require('axios');
const { TOKEN, URL } = process.env;

axios
  .post(\`https://api.telegram.org/bot\$\{TOKEN\}/setWebhook?url=\$\{URL\}/message?token=\$\{TOKEN\}\`)
  .then(response => console.log(response.data.description))
  .catch(error => console.error(error));

`;

const errorHandler = err => (err ? console.error(err) : '');
