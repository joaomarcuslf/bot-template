# Telegram Bot Template

This template focus on the creation of a simple bot boilerplate. The core of the bot will be built as an node api, all you need to do after is focus on yours ideas.

## Getting Started

1. To begin with your bot, please have in hand your bot token: [Bot Reference](https://core.telegram.org/bots)
2. After that, clone this repository with your desired name

```
  $ git clone https://gitlab.com/joaomarcuslf/bot-template.git my-bot-name
  $ cd my-bot-name
  $ yarn
```

3. Now you have your bot token and a generic template, what now

```
  $ yarn start
```

4. Check the git history so you can see what changed

```
  $ git status
```

5. Delete the .git folder and your bot is ready to be coded

```
  $ rm -rf .git/
  $ git add -A && git commit -m "Initial Commit"
```
